import logging

class FetchService:
    def __init__(self, *, producer, client_map):
        self.producer = producer
        self.client_map = client_map

        self.logger = logging.getLogger(__name__)

    async def handle_event(self, event):
        request = event.payload

        self.logger.info('fetching: ' + repr(request))
        client = self.client_map[type(request)]
        response = await client.fetch(request)

        self.logger.info('fetched: ' + repr(response))
        await self.producer.produce(
            topic='scoutbot.response',
            payload=response,
        )

    async def handle_error(self, error, event):
        await self.producer.produce(
            topic='scoutbot.fetch.error',
            payload={
                'error': error,
                'event': event,
            }
        )
