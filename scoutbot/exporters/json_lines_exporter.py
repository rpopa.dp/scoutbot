import json

from scoutbot.interfaces import ExporterInterface

class JsonLinesExporter(ExporterInterface):
    def __init__(self, file):
        self.file = file

    async def export(self, item):
        await self.file.write(json.dumps(item))
        await self.file.write('\n')
