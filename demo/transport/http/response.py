from furl import furl
from functools import cached_property

from scoutbot.interfaces import ResponseInterface

class Response(ResponseInterface):
    def __init__(
        self, *,
        request,
        history,
        url,
        status_code,
        headers,
        cookies,
        body_parsers,
        body_text,
    ):
        self._request = request
        self.history = history
        self.url = furl(url)
        self.status_code = status_code
        self.headers = headers
        self.cookies = cookies
        self.body_parsers = body_parsers
        self.body_text = body_text
        self._body = None

    @cached_property
    def body(self):
        parser = self.body_parsers.for_content_type(
            self.content_type
        )

        return parser.parse(self.body_text)

    @cached_property
    def content_type(self):
        return self.headers['Content-Type'].split(';')[0].strip()

    @property
    def request(self):
        return self._request

    def __repr__(self):
        return (
            f'{type(self).__name__}('
            + f'status_code={repr(self.status_code)}'
            + f', url={repr(self.url)}'
            + f', request={repr(self.request)}'
            + ')'
        )
