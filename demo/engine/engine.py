import asyncio
import logging
import signal
import sys

from scoutbot.interfaces import (
    QueueInterface,
    WorkerInterface,
)

# https://www.roguelynn.com/words/asyncio-graceful-shutdowns/
# https://quantlane.com/blog/ensure-asyncio-task-exceptions-get-logged/
class Engine:
    def __init__(
        self, *,
        fetch_queue: QueueInterface,
        scrape_queue: QueueInterface,
        dump_queue: QueueInterface,
        error_queue: QueueInterface,
        seed_worker: WorkerInterface,
        fetch_worker: WorkerInterface,
        scrape_worker: WorkerInterface,
        dump_worker: WorkerInterface,
        error_worker: WorkerInterface,
    ):
        self.fetch_queue = fetch_queue
        self.scrape_queue = scrape_queue
        self.dump_queue = dump_queue
        self.error_queue = error_queue
        self.seed_worker = seed_worker
        self.fetch_worker = fetch_worker
        self.scrape_worker = scrape_worker
        self.dump_worker = dump_worker
        self.error_worker = error_worker

        self.logger = logging.getLogger(__name__)
        self.loop = asyncio.get_running_loop()

        self.received_shutdown_count = 0
        self.workers_task = None

    async def __aenter__(self):
        self.loop.add_signal_handler(signal.SIGINT, self.stop)
        self.loop.add_signal_handler(signal.SIGTERM, self.stop)

        return self

    async def __aexit__(self, exc_type, exc_value, exc_traceback):
        tasks = [
            task for task in asyncio.all_tasks()
            if task is not asyncio.current_task()
        ]

        try:
            self.logger.info('waiting for tasks to finish')
            await asyncio.gather(*tasks)
        except asyncio.exceptions.CancelledError:
            # NOTE: needed so that tracebacks are printed for errors that
            # happened inside tasks
            pass

    def stop(self):
        self.received_shutdown_count += 1

        if self.received_shutdown_count >= 2:
            sys.exit(1)

        self.logger.info(
            'received termination signal - performing graceful shutdown'
        )
        self.logger.info(
            'send termination signal again to perform a forced shutdown'
        )
        self.loop.create_task(self.shutdown())

    async def run(self):
        await asyncio.gather(
            self.fetch_queue.open(),
            self.scrape_queue.open(),
            self.dump_queue.open(),
            self.error_queue.open(),
        )
        self.workers_task = asyncio.gather(
            self.seed_worker.run(),
            self.fetch_worker.run(),
            self.scrape_worker.run(),
            self.dump_worker.run(),
            self.error_worker.run(),
        )

        await self.workers_task

    async def shutdown(self):
        await asyncio.gather(
            self.seed_worker.shutdown(),
            self.fetch_worker.shutdown(),
            self.scrape_worker.shutdown(),
            self.dump_worker.shutdown(),
            self.error_worker.shutdown(),
        )
        await self.workers_task
        await asyncio.gather(
            self.fetch_queue.close(),
            self.scrape_queue.close(),
            self.dump_queue.close(),
            self.error_queue.close(),
        )
