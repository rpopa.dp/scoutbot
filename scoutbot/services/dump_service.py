import logging

class DumpService:
    def __init__(self, *, producer, exporters):
        self.producer = producer
        self.exporters = exporters

        self.logger = logging.getLogger(__name__)

    async def handle_event(self, event):
        item = event.payload

        for exporter in self.exporters:
            await exporter.export(item)

    async def handle_error(self, error, event):
        await self.producer.produce(
            topic='scoutbot.dump.error',
            payload={
                'error': error,
                'event': event,
            }
        )
