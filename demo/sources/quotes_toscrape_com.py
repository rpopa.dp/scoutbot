from scoutbot.interfaces import (
    SeedRepositoryInterface,
    ExtractorInterface,
)

from transport.http import Request, Response

class SeedRepository(SeedRepositoryInterface):
    async def fetch_seeds(self):
        yield Request(url='http://quotes.toscrape.com/page/1/')

# -----------------------------------------------------------------------------
class Extractor(ExtractorInterface):
    def extract_items(self, response: Response):
        for quote in response.body.select('div.quote'):
            yield QuoteExtractor().extract(response, quote)

    def extract_requests(self, response: Response):
        next_page = response.body.select_one('nav ul.pager li.next a')

        if next_page:
            yield Request(
                url=response.url.origin + next_page['href'],
            )

# -----------------------------------------------------------------------------
class QuoteExtractor:
    def extract(self, response, quote_element):
        return {
            'text': self.extract_text(quote_element),
            'tags': self.extract_tags(response, quote_element),
            'author': self.extract_author(response, quote_element),
        }

    def extract_text(self, quote_element):
        text = quote_element.select_one('span.text').get_text()

        return text[1:-1]

    def extract_tags(self, response, quote_element):
        return [
            {
                'name': tag.get_text(),
                'url': response.url.origin + tag['href'],
            }
            for tag in quote_element.select('div.tags a.tag')
        ]

    def extract_author(self, response, quote_element):
        author_span = quote_element.select_one('span:nth-of-type(2)')

        return {
            'name': author_span.select_one('small.author').get_text(),
            'url': response.url.origin + author_span.select_one('a')['href'],
        }
