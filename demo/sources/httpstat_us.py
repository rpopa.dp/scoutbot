from scoutbot.interfaces import (
    SeedRepositoryInterface,
    ExtractorInterface,
)

from transport.http import Request, Response

class SeedRepository(SeedRepositoryInterface):
    async def fetch_seeds(self):
        yield Request(url='http://httpstat.us/404')

class Extractor(ExtractorInterface):
    def extract_items(self, response: Response):
        if False: yield None

    def extract_requests(self, response: Response):
        if False: yield None
