import abc
from typing import (
    Any,
    AsyncGenerator,
    Generator,
)

class PrioritizableInterface(abc.ABC):
    @property
    @abc.abstractmethod
    def priority(self):
        pass

class DigestableInterface(abc.ABC):
    @property
    @abc.abstractmethod
    def digest(self):
        pass

class WorkerInterface(abc.ABC):
    @abc.abstractmethod
    async def run(self) -> None:
        pass

    @abc.abstractmethod
    async def shutdown(self) -> None:
        pass

class SeedRepositoryInterface(abc.ABC):
    @abc.abstractmethod
    def fetch_seeds(self) -> AsyncGenerator[Any, None]:
        pass

class QueueInterface(abc.ABC):
    @abc.abstractmethod
    async def open(self):
        pass

    @abc.abstractmethod
    async def close(self):
        pass

    @abc.abstractmethod
    async def push(self, item: Any) -> None:
        pass

    @abc.abstractmethod
    def pull(self) -> AsyncGenerator[Any, None]:
        pass

# NOTE: when running a single instance the deduper will work correctly, but in
# distributed environments it may happen that instance B updated the
# deduplication set after instance A called `has()` and before it called `add()`,
# which will cause the instance A to duplicate the request that instance B
# already made
# TODO: [?] use context manager for locking
class DeduperInterface(abc.ABC):
    @abc.abstractmethod
    async def has(self, item: DigestableInterface) -> bool:
        pass

    @abc.abstractmethod
    async def add(self, item: DigestableInterface):
        pass

class ExtractorInterface(abc.ABC):
    @abc.abstractmethod
    def extract_items(self, response) -> Generator[Any, None, None]:
        pass

    @abc.abstractmethod
    def extract_requests(self, response) -> Generator[Any, None, None]:
        pass

class ExporterInterface(abc.ABC):
    @abc.abstractmethod
    async def export(self, item):
        pass

class RequestInterface(abc.ABC):
    @property
    @abc.abstractmethod
    def stage(self):
        pass

class ResponseInterface(abc.ABC):
    @property
    @abc.abstractmethod
    def request(self) -> RequestInterface:
        pass

class ResponseValidatorInterface(abc.ABC):
    @abc.abstractmethod
    def is_valid(self, response: ResponseInterface):
        pass
