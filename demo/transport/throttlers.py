import asyncio
import logging
import time
import typing

class NoopThrottler:
    async def throttle(self, item):
        pass

class FixedDelayThrottler:
    def __init__(self, delay: typing.Union[int, float]):
        self.delay = delay
        self.last_call = None
        self.is_first = True

        self.logger = logging.getLogger(__name__)

    async def throttle(self, item):
        if self.is_first:
            self.is_first = False
        else:
            time_diff = time.monotonic() - self.last_call

            if time_diff < self.delay:
                period = self.delay - time_diff

                self.logger.info(f'throttling for: {period:0.3f} seconds')
                await asyncio.sleep(period)

        self.last_call = time.monotonic()
