from . import Request, Response

# TODO:
#   ? support for socket control
class Client:
    def __init__(
        self, *,
        backend,
        body_parsers,
        throttler,
    ):
        self.backend = backend
        self.body_parsers = body_parsers
        self.throttler = throttler

    async def fetch(self, request: Request) -> Response:
        await self.throttler.throttle(request)

        response = await self.backend.request(
            request.method,
            str(request.url),
            headers=request.headers,
            cookies=request.cookies,
            data=request.data,
        )
        response_cookies = {
            cookie.name: cookie
            for cookie in response.cookies.jar
        }

        return Response(
            request=request,
            history=response.history,
            url=str(response.url),
            status_code=response.status_code,
            headers=response.headers,
            cookies=response_cookies,
            body_parsers=self.body_parsers,
            body_text=response.text,
        )
