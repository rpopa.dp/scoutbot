import logging

class SeedService:
    def __init__(self, *, producer):
        self.producer = producer

        self.logger = logging.getLogger(__name__)

    async def handle_event(self, request):
        await self.producer.produce(
            topic='scoutbot.request',
            payload=request,
        )
        self.logger.info(f'scheduled: {request}')

    async def handle_error(self, error, event):
        await self.producer.produce(
            topic='scoutbot.seed.error',
            payload={
                'error': error,
                'event': event,
            }
        )
