import asyncio
import logging

from scoutbot.interfaces import QueueInterface

class SimpleQueue(QueueInterface):
    def __init__(
        self, *,
        backend,
        label: str,
        pull_timeout: int = 5,
    ):
        self.backend = backend
        self.label = label
        self.pull_timeout = pull_timeout

        self.logger = logging.getLogger(__name__)
        self.is_open = False

    async def open(self):
        self.is_open = True
        self.logger.debug(f'{self.label}: opened')

    async def close(self):
        self.is_open = False
        self.logger.debug(f'{self.label}: closed')

    async def push(self, event):
        if not self.is_open:
            raise Exception('queue is not open')

        self.backend.push(event)

    async def pull(self):
        if not self.is_open:
            raise Exception('queue is not open')

        while self.is_open:
            try:
                event = await asyncio.wait_for(
                    self.pull_event(),
                    timeout=self.pull_timeout,
                )

                yield event
            except asyncio.TimeoutError:
                self.logger.debug(f'{self.label}: pull timed out - closing')
                await self.close()

    async def pull_event(self):
        while self.is_open:
            event = self.backend.pop()

            if event:
                return event

            await asyncio.sleep(0.2)
