from bs4 import BeautifulSoup

class HtmlParser:
    def parse(self, body):
        return BeautifulSoup(body, 'html.parser')

PARSERS = {
    'text/html': HtmlParser(),
}

class BodyParsers:
    def for_content_type(self, content_type):
        parser = PARSERS.get(content_type)

        if not parser:
            raise NotImplementedError(f'no parser for {content_type}')

        return parser
