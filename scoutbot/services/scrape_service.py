import logging

class ScrapeService:
    def __init__(self, *, producer, extractor_map):
        self.producer = producer
        self.extractor_map = extractor_map

        self.logger = logging.getLogger(__name__)

    async def handle_event(self, event):
        response = event.payload
        extractor = self.extractor_map[response.request.stage]

        for item in extractor.extract_items(response):
            await self.producer.produce(
                topic='scoutbot.item',
                payload=item,
            )
            self.logger.info('scraped: ' + repr(item))

        for request in extractor.extract_requests(response):
            await self.producer.produce(
                topic='scoutbot.request',
                payload=request,
            )
            self.logger.info('scheduled: ' + repr(request))

    async def handle_error(self, error, event):
        await self.producer.produce(
            topic='scoutbot.scrape.error',
            payload={
                'error': error,
                'event': event,
            }
        )
