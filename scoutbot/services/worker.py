import logging

from scoutbot.interfaces import WorkerInterface

class Worker(WorkerInterface):
    def __init__(self, *, label, consumer, service):
        self.label = label
        self.consumer = consumer
        self.service = service

        self.is_running = False
        self.logger = logging.getLogger(__name__)

    async def run(self):
        self.logger.debug(f'{self.label} -- running')
        self.is_running = True

        event_iter = self.consumer.__aiter__()

        while self.is_running:
            try:
                event = await event_iter.__anext__()

                await self.service.handle_event(event)
            except StopAsyncIteration:
                await self.shutdown()
                break
            except Exception as error:
                await self.service.handle_error(error, event)

    async def shutdown(self):
        if self.is_running:
            self.logger.debug(f'{self.label} -- shutting down')
            self.is_running = False
