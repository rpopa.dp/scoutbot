import collections
import fnmatch
from datetime import datetime

class Event:
    def __init__(self, *, topic, timestamp, priority, payload):
        self.topic = topic
        self.timestamp = timestamp
        self.priority = priority
        self.payload = payload

    def __repr__(self):
        return (
            f'{type(self).__name__}('
            + f'topic={repr(self.topic)}, '
            + f'timestamp={repr(self.timestamp)}, '
            + f'priority={repr(self.priority)}, '
            + f'payload={repr(self.payload)})'
        )

class Producer:
    def __init__(self, *, event_bus):
        self.event_bus = event_bus

    async def produce(self, *, topic, priority=0, payload):
        await self.event_bus.emit(
            topic=topic,
            event=Event(
                topic=topic,
                timestamp=datetime.utcnow(),
                priority=priority,
                payload=payload,
            ),
        )

class Consumer:
    def __init__(self, *, queue):
        self.queue = queue
        self.iterator = None

    def __aiter__(self):
        self.iterator = self.queue.pull()

        return self

    async def __anext__(self):
        return await self.iterator.__anext__()

class EventBus:
    def __init__(self):
        self.routing = collections.defaultdict(set)

    def build_producer(self):
        return Producer(event_bus=self)

    def build_consumer(self, *, topics, queue):
        for topic in topics:
            self.routing[topic].add(queue)

        return Consumer(queue=queue)

    async def emit(self, *, topic, event):
        for routing_topic in self.routing.keys():
            if not fnmatch.fnmatch(topic, routing_topic):
                continue

            for queue in self.routing[routing_topic]:
                await queue.push(event)
