from scoutbot.interfaces import DeduperInterface, DigestableInterface

class SimpleDeduper(DeduperInterface):
    def __init__(self):
        self.backend = set()

    async def has(self, item: DigestableInterface) -> bool:
        return item.digest in self.backend

    async def add(self, item: DigestableInterface):
        self.backend.add(item.digest)
