import collections

class SimpleBackend:
    def __init__(self):
        self.queue = []

    def push(self, event):
        self.queue.append(event)

    def pop(self):
        try:
            return self.queue.pop(0)
        except IndexError:
            return None

class PriorityBackend:
    def __init__(self):
        self.queues = collections.defaultdict(list)

    def push(self, event):
        self.queues[-event.priority].append(event)

    def pop(self):
        try:
            min_priority = min(self.queues.keys(), default=0)

            return self.queues[min_priority].pop(0)
        except IndexError:
            self.queues.pop(min_priority)
            return None
