import inspect

def get_exc_info(exc):
    frames = inspect.getinnerframes(exc.__traceback__)

    return {
        'type': str(type(exc)),
        'message': str(exc),
        'traceback': [
            f'{frame.filename}:{frame.lineno}'
            for frame in frames
        ],
    }

def format_exc_info(exc_info):
    formatted_frames = '\n'.join(exc_info['traceback'])

    return (
        f"Exception: {exc_info['type']}\n"
        + f"Message: {exc_info['message']}\n"
        + f"Trace:\n{formatted_frames}"
    )

def format_exc(exc):
    return format_exc_info(get_exc_info(exc))

# -----------------------------------------------------------------------------
import random
import string

RANDOM_STRING_CHARS = string.ascii_letters + string.digits

def random_string(length):
    return ''.join(random.choice(RANDOM_STRING_CHARS) for _ in range(length))
