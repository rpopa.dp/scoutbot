import csv

from scoutbot.interfaces import ExporterInterface

# NOTE: doesn't handle the cases when item's values are not primitives
class CsvExporter(ExporterInterface):
    def __init__(self, file):
        self.writer = csv.writer(file, quoting=csv.QUOTE_ALL)
        self.header_was_written = False

    async def export(self, item):
        if not self.header_was_written:
            self.writer.writerow([str(key) for key in item.keys()])
            self.header_was_written = True

        self.writer.writerow([str(value) for value in item.values()])
