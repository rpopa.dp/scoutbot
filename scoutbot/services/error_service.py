import logging

from scoutbot.utils import format_exc

class ErrorService:
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    async def handle_event(self, event):
        self.logger.error(f'Error while processing event: {repr(event)}')
        self.logger.error(format_exc(event.payload['error']))

    async def handle_error(self, error, event):
        pass
