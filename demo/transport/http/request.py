import hashlib
from furl import furl

from scoutbot.interfaces import (
    RequestInterface,
    DigestableInterface,
)

class Request(
    RequestInterface,
    DigestableInterface,
):
    def __init__(
        self, *,
        method='GET',
        url,
        headers={},
        cookies={},
        data=None,
        stage='__default__',
    ):
        self.method = method
        self.url = furl(url)
        self.headers = headers
        self.cookies = cookies
        self.data = data
        self._stage = stage

    @property
    def stage(self):
        return self._stage

    @property
    def digest(self):
        hash_ctx = hashlib.sha256()

        # TODO: [?] digest headers and data too
        hash_ctx.update(str(self.method).encode('utf-8'))
        hash_ctx.update(str(self.url).encode('utf-8'))

        return hash_ctx.hexdigest()

    def __repr__(self):
        return (
            f'{type(self).__name__}('
            + f'stage={repr(self.stage)}'
            + f', method={repr(self.method)}'
            + f', url={repr(self.url)}'
            + ')'
        )
