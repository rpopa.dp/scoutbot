import asyncio
import contextlib
import httpx
import logging
import sys

from scoutbot.events import EventBus
from scoutbot.exporters import JsonLinesExporter
from scoutbot.queues import SimpleQueue, SimpleBackend, PriorityBackend
from scoutbot.services import (
    Worker,
    SeedService,
    FetchService,
    ScrapeService,
    DumpService,
    ErrorService,
)

from transport.http import (
    Request as HttpRequest,
    Client as HttpClient,
    BodyParsers as HttpBodyParsers,
)
from transport.throttlers import FixedDelayThrottler

from engine import Engine

import sources.quotes_toscrape_com as source

def setup_logging():
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.DEBUG,
        format='[%(asctime)s|%(name)s|%(levelname)s] %(message)s',
    )

class FileOutput:
    def __init__(self, filename):
        self.filename = filename
        self.file = None

        self.logger = logging.getLogger(__name__)

    async def __aenter__(self):
        self.file = open(self.filename, 'w')
        self.logger.debug(f'opened file {repr(self.filename)} for writing')

        return self

    async def __aexit__(self, exc_type, exc_value, exc_traceback):
        self.file.flush()
        self.file.close()
        self.logger.debug(f'closed file {repr(self.filename)}')

    async def write(self, string):
        self.file.write(string)

async def main():
    async with contextlib.AsyncExitStack() as exit_stack:
        jsonl_output_file = await exit_stack.enter_async_context(
            FileOutput(filename='output.jsonl')
        )
        http_backend = await exit_stack.enter_async_context(
            httpx.AsyncClient()
        )

        # ---------------------------------------------------------------------
        fetch_queue = SimpleQueue(
            label='fetch-queue',
            backend=PriorityBackend(),
        )
        scrape_queue = SimpleQueue(
            label='scrape-queue',
            backend=SimpleBackend(),
        )
        dump_queue = SimpleQueue(
            label='dump-queue',
            backend=SimpleBackend(),
        )
        error_queue = SimpleQueue(
            label='error-queue',
            backend=SimpleBackend(),
        )

        # ---------------------------------------------------------------------
        event_bus = EventBus()
        producer = event_bus.build_producer()

        # ---------------------------------------------------------------------
        seeds_consumer = source.SeedRepository().fetch_seeds()
        seed_service = SeedService(producer=producer)
        seed_worker = Worker(
            label='seeder',
            consumer=seeds_consumer,
            service=seed_service,
        )

        # ---------------------------------------------------------------------
        requests_consumer = event_bus.build_consumer(
            topics=['scoutbot.request'],
            queue=fetch_queue,
        )
        fetch_service = FetchService(
            producer=producer,
            client_map={
                HttpRequest: HttpClient(
                    backend=http_backend,
                    body_parsers=HttpBodyParsers(),
                    throttler=FixedDelayThrottler(delay=1),
                ),
            },
        )
        fetch_worker = Worker(
            label='fetcher',
            consumer=requests_consumer,
            service=fetch_service,
        )

        # ---------------------------------------------------------------------
        responses_consumer = event_bus.build_consumer(
            topics=['scoutbot.response'],
            queue=scrape_queue,
        )
        scrape_service = ScrapeService(
            producer=producer,
            extractor_map={
                '__default__': source.Extractor(),
            },
        )
        scrape_worker = Worker(
            label='scraper',
            consumer=responses_consumer,
            service=scrape_service,
        )

        # ---------------------------------------------------------------------
        items_consumer = event_bus.build_consumer(
            topics=['scoutbot.item'],
            queue=dump_queue,
        )
        dump_service = DumpService(
            producer=producer,
            exporters=[
                JsonLinesExporter(file=jsonl_output_file),
            ],
        )
        dump_worker = Worker(
            label='dumper',
            consumer=items_consumer,
            service=dump_service,
        )

        # ---------------------------------------------------------------------
        errors_consumer = event_bus.build_consumer(
            topics=['scoutbot.*.error'],
            queue=error_queue,
        )
        error_service = ErrorService()
        error_worker = Worker(
            label='dumper',
            consumer=errors_consumer,
            service=error_service,
        )

        # ---------------------------------------------------------------------
        engine = await exit_stack.enter_async_context(
            Engine(
                fetch_queue=fetch_queue,
                scrape_queue=scrape_queue,
                dump_queue=dump_queue,
                error_queue=error_queue,
                seed_worker=seed_worker,
                fetch_worker=fetch_worker,
                scrape_worker=scrape_worker,
                dump_worker=dump_worker,
                error_worker=error_worker,
            )
        )

        await engine.run()

if __name__ == '__main__':
    setup_logging()

    asyncio.run(main(), debug=False)
